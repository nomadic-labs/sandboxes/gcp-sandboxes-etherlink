## Search and replace "nickname" => your server name (avoid '-' and '_')
## copy this file to nickname.tf

module "server_snapshotter" {
  # cf. https://gitlab.com/nomadic-labs/sandboxes/tf-gcp-sandbox-module/-/infrastructure_registry
  source  = "gitlab.com/nomadic-labs/tf-gcp-sandbox-module/local"
  version = "2.3.1" # cf. https://gitlab.com/nomadic-labs/sandboxes/tf-gcp-sandbox-module/-/tags

  # Instance name (without '_')
  instance_name = "snapshotter"

  # Machine type, cf. https://cloud.google.com/compute/docs/machine-types"
  machine_type = "e2-highmem-4"

  # OS Image, cf. https://gitlab.com/nomadic-labs/sandboxes README for tested values
  image = "ubuntu-os-cloud/ubuntu-minimal-2404-noble-amd64-v20241116"

  # ssh_username: ssh keys with be added to this user (default : nomadic)
  ssh_username = "ubuntu"

  # gitlab handles of users to add, from https://gitlab.com/<user>.keys
  gitlab_users = ["vch9", "sribaroud", "lthms"]

  network_self_link    = google_compute_network.ipv6net.self_link
  subnetwork_self_link = google_compute_subnetwork.ipv6subnet.self_link

  # in GB, Book disk should be at least the size of your image
  boot_disk_size = 50
  # in GB, mounted on /opt
  attached_disk_size = 4000
  # ghostnet EVM can runs in a machine with 500GB but we are at 90% capacity.
  # where I'll copy the snapshot 1 snapshot is about 400Gb, I would like to have at least 2

  # user_setup_script: commands run by root at startup
  user_setup_script = <<EOH
echo "hello world" >> /tmp/hello_world.txt

EOH

  # DNS : (defaults to "${var.instance_name}-sandbox.nomadic-labs.eu") 
  # zone names available: "nomadic-labs-eu" "octez-tech" "octez-online"
  # dns_zone_name          = "nomadic-labs-eu"
  # use_custom_record_name = true
  # custom_record_name     = ""

  # for storage bucket access and ops monitoring
  additional_scopes = ["storage-rw"]
}

output "server_snapshotter_ssh" {
  description = "ssh command to connect to the server's address if your ssh key is in the list"
  value       = format("ssh %s@%s", module.server_snapshotter.ssh_username, trimsuffix(module.server_snapshotter.instance_dns_name, "."))
}

output "server_snapshotter_IPv4" {
  description = "Ephemeral IP v4 allocated to this server"
  value       = module.server_snapshotter.ipv4_address
}

output "server_snapshotter_IPv6" {
  description = "IP v6 allocated to this server"
  value       = module.server_snapshotter.ipv6_address
}
