# backend on GCS

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "6.24.0"
    }
  }
  backend "gcs" {
    bucket = "nl-sandboxes-etherlink-tfstate"
    prefix = "terraform/state"
  }
}
