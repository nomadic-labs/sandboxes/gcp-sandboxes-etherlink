# Define instances with bucket access
locals {
  instances = {
    instance1 = module.server_snapshotter
    # Add more instances as needed
  }
}

resource "google_storage_bucket" "etherlink_snapshots_bucket" {
  name          = "nl-sandboxes-etherlink--snapshots"
  location      = "EUROPE-WEST3"
  force_destroy = true

  # https://cloud.google.com/storage/docs/uniform-bucket-level-access
  uniform_bucket_level_access = true

  # https://cloud.google.com/storage/docs/public-access-prevention
  # public_access_prevention = "enforced"

  versioning {
    enabled = true
  }

  labels = {
    env = "sandbox"
  }
}

# Dynamic IAM bindings for multiple instances

# objectViewer + objectCreator => list, copy, create, but not allowed to delete
#resource "google_storage_bucket_iam_member" "bucket_readers" {
#  for_each = local.instances
#
#  bucket = google_storage_bucket.etherlink_snapshots_bucket.name
#  role   = "roles/storage.objectViewer"
#  member = "serviceAccount:${each.value.service_account_email}"
#}

#resource "google_storage_bucket_iam_member" "bucket_writers" {
#  for_each = local.instances
#
#  bucket = google_storage_bucket.etherlink_snapshots_bucket.name
#  role   = "roles/storage.objectCreator"
#  member = "serviceAccount:${each.value.service_account_email}"
#}

# objectUser : view, create, and delete
resource "google_storage_bucket_iam_member" "snapshot_deleters" {
  for_each = local.instances

  bucket = google_storage_bucket.etherlink_snapshots_bucket.name
  role   = "roles/storage.objectUser"
  member = "serviceAccount:${each.value.service_account_email}"
}


output "gcs_bucket_name" {
  value       = google_storage_bucket.etherlink_snapshots_bucket.name
  description = "The name of the Google Cloud Storage bucket"
}

data "google_iam_policy" "allUsersViewers" {
  binding {
    role = "roles/storage.objectViewer"
    members = [
      "allUsers",
    ]
  }
}


resource "google_storage_managed_folder" "ghostnet" {
  bucket        = google_storage_bucket.etherlink_snapshots_bucket.name
  name          = "etherlink-ghostnet/"
  force_destroy = false
}

resource "google_storage_managed_folder_iam_policy" "ghostnet_policy" {
  bucket         = google_storage_managed_folder.ghostnet.bucket
  managed_folder = google_storage_managed_folder.ghostnet.name
  policy_data    = data.google_iam_policy.allUsersViewers.policy_data
}

resource "google_storage_managed_folder" "testnet" {
  bucket        = google_storage_bucket.etherlink_snapshots_bucket.name
  name          = "etherlink-testnet/"
  force_destroy = false
}

resource "google_storage_managed_folder_iam_policy" "testnet_policy" {
  bucket         = google_storage_managed_folder.testnet.bucket
  managed_folder = google_storage_managed_folder.testnet.name
  policy_data    = data.google_iam_policy.allUsersViewers.policy_data
}

resource "google_storage_managed_folder" "mainnet" {
  bucket        = google_storage_bucket.etherlink_snapshots_bucket.name
  name          = "etherlink-mainnet/"
  force_destroy = false
}

resource "google_storage_managed_folder_iam_policy" "mainnet_policy" {
  bucket         = google_storage_managed_folder.mainnet.bucket
  managed_folder = google_storage_managed_folder.mainnet.name
  policy_data    = data.google_iam_policy.allUsersViewers.policy_data
}