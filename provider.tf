# provider.tf

provider "google" {
  ## (using default from environment variables)
  #  project = var.GCLOUD_PROJECT 
  #  region  = var.GCLOUD_REGION
  #  zone    = var.GCLOUD_ZONE

  ## TODO :  cf. https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/provider_reference#add_terraform_attribution_label
  # Label goog-terraform-provisioned = true will be added automatically
  #  add_terraform_attribution_label               = true
  #  terraform_attribution_label_addition_strategy = "PROACTIVE"
}

resource "google_project_service" "compute_service" {
  # project = var.GCLOUD_PROJECT
  service = "compute.googleapis.com"

  timeouts {
    create = "30m"
    update = "40m"
  }

  disable_dependent_services = true
  # lifecycle {
  #   prevent_destroy = true
  # }
}